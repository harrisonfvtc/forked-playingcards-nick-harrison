#include <conio.h>
#include <iostream>

using namespace std;

enum Suit 
{
	SPADES,
	DIAMONDS,
	CLUBS,
	HEARTS
};

enum Rank
{
	TWO,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

struct Card
{
	Suit suit;
	Rank rank;
};

Card HighCard(Card card1, Card card2)
{
	if (card1.rank > card2.rank)
	{
		return card1;
	}
	
	else if (card1.rank < card2.rank)
	{
		return card2;
	}
	else
	{
		if (card1.suit > card2.suit)
		{
			return card1;
		}
		else
		{
			return card2;
		}
	}
}

void PrintCard(Card card)
{
	string rankings = "", suits = "";
	switch (card.rank)
	{
		case TWO: rankings = "2"; break;
		case THREE: rankings = "3"; break;
		case FOUR: rankings = "4"; break;
		case FIVE: rankings = "5"; break;
		case SIX: rankings = "6"; break;
		case SEVEN: rankings = "7"; break;
		case EIGHT: rankings = "8"; break;
		case NINE: rankings = "9"; break;
		case TEN: rankings = "10"; break;
		case JACK: rankings = "Jack"; break;
		case QUEEN: rankings = "Queen"; break;
		case KING: rankings = "King"; break;
		case ACE: rankings = "Ace"; break;


	}

	switch (card.suit)
	{
		case CLUBS: suits = "Clubs"; break;
		case DIAMONDS: suits = "Diamonds"; break;
		case HEARTS: suits = "Hearts"; break;
		case SPADES: suits = "Spades"; break;
	}

	cout << rankings << " Of " << suits << endl;
		
}

int main()
{
	Card card1, card2;
	
	card1.rank = FIVE;
	card1.suit = DIAMONDS;

	card2.rank = KING;
	card2.suit = SPADES;
	
	cout << "Card 1: ";
	PrintCard(card1);
	cout << "Card 2: ";
	PrintCard(card2);
	cout << "High card: ";
	PrintCard(HighCard(card1, card2));
	return 0;

	_getch();
	return 0;
}